import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class BaseSteps {

    WebDriver driver;

    JavascriptExecutor js;

    String BASE_URL;

    protected WebDriver getDriver() {

        return driver;

    }

    public void startUp(){

        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");

        driver = new ChromeDriver();

        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

        driver.get(BASE_URL);

        js = ((JavascriptExecutor) driver);
    }

    public void endTest(){

        driver.quit();

    }

    void waitForVisible(By locator){

        WebDriverWait wait = new WebDriverWait(driver, 10);

        wait.pollingEvery(Duration.ofMillis(300))
                .until(ExpectedConditions.visibilityOfElementLocated(locator));

    }

    void selectByText(WebElement element, String text){

        new Select(element).selectByVisibleText(text);

    }
    WebElement findByXpath (String xpath){return findByLocator(By.xpath(xpath));}

    WebElement findByLocator (By locator){
        return driver.findElement(locator);
    }

    void click(By locator){

        //waitForVisible(locator);

        findByLocator(locator).click();

    }

    void click(String xpath){

        //waitForVisible(By.xpath(xpath));

        findByXpath(xpath).click();

    }

    void checkElementContains(WebElement element, String expectedText){

    }

    void checkElementEquals(WebElement element, String expectedText){

        Assert.assertEquals("Значения текста не соответствуют содержимому",
                expectedText, element.getText());

    }

    void fillElement(By locator, String text){

        waitForVisible(locator);

        WebElement element = findByLocator(locator);
        if(element.getTagName().equalsIgnoreCase("select")){
            selectByText(element, text);
        }
        else {
            click(locator);
            element.sendKeys(text);
        }
    }

    void fillElement(String xpath, String text){

        fillElement(By.xpath(xpath), text);

    }

    void jsScroll(By locator){

        js.executeScript("arguments[0].scrollIntoView(true);", findByLocator(locator));

    }

    boolean isElementPresented(String xpath){
        try {
            waitForVisible(By.xpath(xpath));
        }
        catch (NoSuchElementException ex){
            return false;
        }
        catch  (TimeoutException e) {
            return false;
        }
        return driver.findElement(By.xpath(xpath)).isDisplayed();
    }



}

