import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

@RunWith(Parameterized.class)
public class PrettyAutotest2 {

    RgsSteps user = new RgsSteps();

    @Before
    public void before(){
        user.startUp();
    }

    @After
    public void after(){
        user.endTest();
    }
    @Parameterized.Parameters
    public static List<Object[]> isTrueData(){
        return Arrays.asList(new Object[][]{
                {"Шенген", true},
                {"Многократные поездки", true},
                {"IVANOV IVAN", true},
                {LocalDate.now().plusDays(7).format(DateTimeFormatter.ofPattern("dd.MM.YYYY", Locale.GERMAN)), true},
                {LocalDate.now().minusYears(18).format(DateTimeFormatter.ofPattern("dd.MM.YYYY", Locale.GERMAN)), true},
                {"Испания", false},
                {"PETROV", false},
        });
    }

    @Parameterized.Parameter
    public String message;

    @Parameterized.Parameter(1)
    public boolean errorResult;

    @Test
    public void checkErrorsOnlineCalculateForm() throws InterruptedException {

        user.openInsuranceNavBar();

        user.chooseCategory("Путешествия","Выезжающим за рубеж");

        user.checkTripHeader("Заголовок страницы", "Страхование выезжающих за рубеж");

        user.openOnlineCalculationForm();

        user.fillCommonInfo();

        user.fillCalculateForm("Шенген", "Испания", LocalDate.now().plusDays(7).format(DateTimeFormatter.ofPattern("dd.MM.YYYY", Locale.GERMAN)), "IVANOV IVAN",  LocalDate.now().minusYears(18).format(DateTimeFormatter.ofPattern("dd.MM.YYYY", Locale.GERMAN)));

        user.clickSubmitButton();

        user.checkErrorsFromMessage(message, errorResult);

       /* user.checkErrorsFromMessage("Многократные поездки");

        user.checkErrorsFromMessage("Шенген");

        user.checkErrorsFromMessage("IVANOV IVAN");

        user.checkErrorsFromMessage(LocalDate.now().plusDays(7).format(DateTimeFormatter.ofPattern("dd.MM.YYYY", Locale.GERMAN)));

        user.checkErrorsFromMessage(LocalDate.now().minusYears(18).format(DateTimeFormatter.ofPattern("dd.MM.YYYY", Locale.GERMAN)));
*/
    }
}
