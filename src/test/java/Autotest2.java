/**
 * Сценарий №2
 * 1. Перейти по ссылке http://www.rgs.ru
 * 2. Выбрать пункт меню – Страхование
 * 3. Путешествия – Страхование выезжающих за рубеж
 * 4. Нажать рассчитать – Онлайн
 * 5. Проверить наличие заголовка – Страхование выезжающих за руюеж
 * 6. Заполнить форму:
 * Несколько поездок в течении года
 * Я согласен на обработку данных  - выбрать чекбокс
 * 7. Нажать рассчитать
 * 8. Заполнить поля:
 * Куда едем – Шенген
 * Страна въезда – Испания
 * Дата первой поездки – 1 ноября
 * Сколько дней планируете пробыть за рубежом за год – не более 90
 * ФИО
 * Дата рождения
 * Планируется активный отдых
 * 9. Нажать рассчитать
 * 10. Проверить значения:
 * Условия страхования – Многократные поездки в течении года
 * Территория – Шенген
 * Застрахованный
 * Дата рождения
 * Активный отдых - включен
 */

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;

public class Autotest2 {

    WebDriver driver;
    @Before
    public void startDriver(){

        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");

        driver = new ChromeDriver();

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--header");
        driver.manage().window().maximize();

        //change waiting

        driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);//лучше явные ожидания

       //Step 1

        driver.get("https://www.rgs.ru");
    }

    //@BeforeClass//1 time before all tests
    @After
    public void closeDriver(){
        //if(driver==null);
        driver.quit();
    }
    @Test

    public void TestRgs() throws InterruptedException {

        WebDriverWait wait = new WebDriverWait(driver, 10);
        //Step 2
        WebElement insuranceNavBarButton = findByXpath("//*[@id='main-navbar-collapse']//a[contains(text(), 'Страхование')]");
        insuranceNavBarButton.click();

        //Step 3
        WebElement insuranceLink = findByXpath("//div[@class='rgs-main-menu-category']//a[contains(text(), " +
                "'Путешествия')]/parent::*/following-sibling::*/li/a[contains(text(), \"Выезжающим за рубеж\")]");
        insuranceLink.click();

        //Step 5

        //WebElement pageHeader = findByXpath("//div[@class='page-header']/descendant-or-self::*[contains (text(), \"Страхование выезжающих за рубеж\")]");
        WebElement pageHeader = findByXpath("//text()[contains(.,'Страхование выезжающих за') and contains(., 'рубеж')]//ancestor-or-self::*[self::div[@class='page-header']]");
        String expectedText = "Страхование выезжающих за рубеж";
        Assert.assertThat("They are not equal!", expectedText, is(pageHeader.getText()));

        //Step 4
        JavascriptExecutor js = ((JavascriptExecutor) driver);

        WebElement calculateButton = findByXpath("//text()[contains(., 'Онлайн')]//ancestor::div//child::a[contains(text(), 'Рассчитать')]");

        js.executeScript("arguments[0].scrollIntoView(true);", calculateButton);
/*        wait.pollingEvery(Duration.ofMillis(300))
                .until(ExpectedConditions.visibilityOf(calculateButton));*/
        calculateButton.click();

        //Step 6
        wait.pollingEvery(Duration.ofMillis(300))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//text()[contains(., ' Несколько ')]//ancestor::button")));;

        WebElement multiTripButton = findByXpath("//text()[contains(., ' Несколько ')]//ancestor::button");

        wait.pollingEvery(Duration.ofMillis(300))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//text()[contains(., 'Я согласен')]/ancestor::label[@class = 'adaptive-checkbox-label']")));;

        WebElement checkBox = findByXpath("//text()[contains(., 'Я согласен')]/ancestor::label[@class = 'adaptive-checkbox-label']");
        checkBox.click();

        multiTripButton.click();

        //Step 7

        WebElement countButton = findByXpath("//text()[contains(., ' Рассчитать ')]//ancestor::button[@type = 'submit']");
        js.executeScript("arguments[0].scrollIntoView(true);", countButton);
        countButton.click();

        //Step 8
        WebElement inputCountry = findByXpath ("//input[@placeholder ='Укажите одну или несколько стран']");

        inputCountry.sendKeys("Шенген");

        wait.pollingEvery(Duration.ofMillis(300))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//text()[contains (., 'Шенген')]/ancestor::multiple-autocomplete")));;

        WebElement chooseCountry = findByXpath ("//text()[contains (., 'Шенген')]/ancestor::multiple-autocomplete");

        chooseCountry.click();

        wait.pollingEvery(Duration.ofMillis(300))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id = 'ArrivalCountryList']")));

        WebElement countriesForSelect = findByXpath("//select[@id = 'ArrivalCountryList']");

        Select chooseRegion = new Select(countriesForSelect);

        chooseRegion.selectByVisibleText("Испания");

        wait.pollingEvery(Duration.ofMillis(300))
               .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//text()[contains(., ' Дата первой поездки ')]//following::input[1]")));

        WebElement inputDate = findByXpath ("//text()[contains(., ' Дата первой поездки ')]//following::input[1]");

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd.MM.YYYY", Locale.GERMAN);

        String date = LocalDate.now().plusDays(7).format(dateFormat);

        inputDate.click();

        inputDate.sendKeys(date);

        wait.pollingEvery(Duration.ofMillis(300))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//text()[contains(., ' Не более 90 дней ')]//parent::label")));

        WebElement howMuchDaysButton = findByXpath("//text()[contains(., ' Не более 90 дней ')]//parent::label");

        howMuchDaysButton.click();

        WebElement fullNameInput = findByXpath("//div[@class = 'panel panel-default']//input[@placeholder ='NIKOLAEV NIKOLAY'] ");

        fullNameInput.click();

        fullNameInput.sendKeys("IVANOV IVAN");

        WebElement dayOfBirthday = findByXpath("//label[contains(text(), 'Дата рождения' )]//following-sibling::input[@placeholder = 'ДД.ММ.ГГГГ']");

        dayOfBirthday.click();

        dayOfBirthday.sendKeys("01.01.1990");

        WebElement checkTypeTrip = findByXpath("//a[contains(text(), 'активный отдых')]//parent::*//preceding-sibling::div");

        js.executeScript("arguments[0].scrollIntoView(true);", checkTypeTrip);

        checkTypeTrip.click();

        //Step 9
        WebElement submitButton = findByXpath("//text()[contains(., 'Рассчитать')]//ancestor::button[@type = 'submit']");

        js.executeScript("arguments[0].scrollIntoView(true);", submitButton);

        submitButton.click();

        //Step 10

       /* wait.pollingEvery(Duration.ofMillis(300))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()[contains(., 'Хороший выбор. Наша самая популярная программа')]]")));
        */
       Thread.sleep(4000);

        List<String> listSummaryValue = driver.findElements(By.xpath(
                "//*[@class = 'summary']/descendant::*[@class = 'summary-row' and not(ancestor::*[contains(@style, 'display: none')]) " +
                        "and not(self::*[contains(@style, 'display: none')])]//*[@class ='summary-value']"))
                .stream().map(WebElement::getText).collect(Collectors.toList());

        Thread.sleep(4000);

        Assert.assertTrue("Trip is not true",  listSummaryValue.get(0).contains("Многократные поездки"));
        Assert.assertTrue("Date is not true", listSummaryValue.get(1).contains(date));
        Assert.assertTrue("Type is not true", listSummaryValue.get(2).contains("Шенген"));
        Assert.assertTrue("Full name is not true", listSummaryValue.get(3).contains("IVANOV IVAN"));
        Assert.assertTrue("Day of birthday is not true", listSummaryValue.get(3).contains("01.01.1990"));
        Assert.assertTrue("is not Active", listSummaryValue.get(4).contains("активный отдых"));
    }

    private WebElement findByXpath (String xpath){
        return driver.findElement(By.xpath(xpath));
    }

}
