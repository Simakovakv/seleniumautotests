import org.junit.Assert;
import org.openqa.selenium.*;

import java.util.HashMap;
import java.util.Map;

class RgsSteps extends BaseSteps{

    private static final String BASE_RGS_URL = "https://www.rgs.ru";

    private static final By openInsuranceNavBarButtonLocator = By.xpath("//*[@id='main-navbar-collapse']//a[contains(text(), 'Страхование')]");
//@FindBy(xpath = "//*[@id='main-navbar-collapse']//a[contains(text(), 'Страхование')]";
//    private WebElement openInsuranceNavBar;

    private static final By navBarLocator = By.className("container-rgs-main-menu-links");

    private static final String categoryFormat= "//div[@class='rgs-main-menu-category']//a[contains(text(), " +
            "'%s')]/parent::*/following-sibling::*/li/a[contains(text(), '%s')]";

    private static final By onlineCalculationButton = By.xpath("//text()[contains(., 'Онлайн')]//ancestor::div//child::a[contains(text(), 'Рассчитать')]");

    private static final By headerCalculateLocator = By.className("container container-rgs-app-title");

    private static final By fullNameInput = By.xpath("//div[@class = 'panel panel-default']//input[@placeholder ='NIKOLAEV NIKOLAY']");

    private static final By dayOfBirthdayInput = By.xpath("//label[contains(text(), 'Дата рождения' )]//following-sibling::input[@placeholder = 'ДД.ММ.ГГГГ']");

    private static final By chooseCountryInput = By.xpath("//input[@placeholder ='Укажите одну или несколько стран']");

    private static final By chooseCountryDropdown = By.xpath("//multiple-autocomplete//descendant::div[@role = 'presentation']");

    private static final By chooseArrivalCountryInput = By.xpath("//select[@id = 'ArrivalCountryList']");

    private static final By firstTripDateInput = By.xpath("//text()[contains(., ' Дата первой поездки ')]//following::input[1]");

    private static final By howMuchDaysButton = By.xpath("//text()[contains(., ' Не более 90 дней ')]/parent::*");

    private static final By checkTypeTripButton = By.xpath("//a[contains(text(), 'активный отдых')]//parent::*//preceding-sibling::div");

    private static final By submitButton = By.xpath("//text()[contains(., 'Рассчитать')]//ancestor::button[@type = 'submit']");

    private static final By multiTripButton = By.xpath("//text()[contains(., ' Несколько ')]//ancestor::button");

    private static final By agreeCheckBox = By.xpath("//text()[contains(., 'Я согласен')]/ancestor::label[@class = 'adaptive-checkbox-label']");


    private static final String errorPattern = "//*[@class = 'summary']/descendant::*[@class = 'summary-row' and not(ancestor::*[contains(@style, 'display: none')]) and not(self::*[contains(@style, 'display: none')])]//*[@class ='summary-value']//*[text()[contains(., '%s')]]";

    Map<String, By> locatorsMap = new HashMap<String, By>() {
        {

        put("Заголовок страницы", By.xpath("//text()[contains" +
                "(.,'Страхование выезжающих за') and contains(., 'рубеж')]//ancestor-or-self::*[self::div[@class='page-header']]"));
        }

    };

    public RgsSteps(){

        BASE_URL = BASE_RGS_URL;

    }
   /* @Override
    public void startUp(String baseUrl){
        super.startUp(BASE_RGS_URL);
    }*/
    public void openInsuranceNavBar(){

        click(openInsuranceNavBarButtonLocator);

        waitForVisible(navBarLocator);

    }

    public void chooseCategory(String categoryName, String subCategoryName){

        By categoryLocator = By.xpath(String.format(categoryFormat, categoryName, subCategoryName));

        click(categoryLocator);

    }

    //??User (parametrized: login, password)
    //Step 5
    public void checkTripHeader(String headerName, String expectedText){

        WebElement header = findByLocator(locatorsMap.get(headerName));

        checkElementEquals(header, expectedText);

    }

    public void fillCommonInfo(){

        waitForVisible(agreeCheckBox);
        click(agreeCheckBox);

        waitForVisible(multiTripButton);
        click(multiTripButton);

    }

    public void openOnlineCalculationForm(){

        jsScroll(onlineCalculationButton);

        click(onlineCalculationButton);

        //waitForVisible(headerCalculateLocator);

    }

    public void fillCalculateForm(String inputCountry, String inputArrivalCountry, String inputDate, String fullName, String dayOfBirthday){


        fillElement(chooseCountryInput, inputCountry);

        click(chooseCountryDropdown);

        fillElement(chooseArrivalCountryInput, inputArrivalCountry);

        fillElement(firstTripDateInput, inputDate);

        waitForVisible(howMuchDaysButton);

        jsScroll(howMuchDaysButton);

        click(howMuchDaysButton);

        fillElement(fullNameInput, fullName);

        fillElement(dayOfBirthdayInput, dayOfBirthday);

        jsScroll(checkTypeTripButton);

        click(checkTypeTripButton);

    }

    public void clickSubmitButton(){

        jsScroll(submitButton);

        click(submitButton);

    }

    public void checkErrorsFromMessage(String expectedMessage) throws InterruptedException {

        waitForVisible(By.xpath(String.format(errorPattern, expectedMessage)));

        Assert.assertTrue(String.format("Value '%s' is not true", expectedMessage), isElementPresented(String.format(errorPattern, expectedMessage)));

    }

    public void checkErrorsFromMessage(String message, boolean expectedResult){

        Assert.assertEquals(isElementPresented(String.format(errorPattern, message)), expectedResult);
    }

}
